'''
Zachary Hardin 11/17/2016
Followed this tutorial: https://first-web-scraper.readthedocs.io/en/latest/#prelude-prerequisites
'''

import csv
import requests
from BeautifulSoup import BeautifulSoup


def save_data_to_excel_sheet(list_of_rows):
    output = open("./inmates.csv", "wb")
    writer = csv.writer(output)
    writer.writerow(["Last", "First", "Middle", "Gender", "Race", "Age", "City", "State"])
    writer.writerows(list_of_rows)


def extract_data_from_html(table):
    list_of_rows = []

    for row in table.findAll('tr'):
        list_of_cells = []
        for cell in row.findAll('td')[1:]:
            text = cell.text.replace('&nbsp;', '')
            list_of_cells.append(text)
        list_of_rows.append(list_of_cells)

    return list_of_rows


def get_html_content():
    url = 'http://www.showmeboone.com/sheriff/JailResidents/JailResidents.asp'
    response = requests.get(url)
    html = response.content
    return html


def extract_tbody_from_html(html):
    soup = BeautifulSoup(html)
    table = soup.find('tbody', attrs={'class': 'stripe'})
    return table


def main():
    html = get_html_content()
    table = extract_tbody_from_html(html)

    list_of_rows = extract_data_from_html(table)
    save_data_to_excel_sheet(list_of_rows)

main()