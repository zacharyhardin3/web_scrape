# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Experimenting with web scraping. The turorial below was really useful and educational. Check it out!
https://first-web-scraper.readthedocs.io/en/latest/#prelude-prerequisites

### How do I get set up? ###

Make sure to install the following items:
	pip				(latest version)
	python 			(at least 2.7 or higher)
	requests 		(sudo pip install Requests)
	BeautifulSoup 	(sudo pip install BeautifulSoup)